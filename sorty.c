#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

typedef struct POC{
    int vymen;
    int porov;
}POC;

FILE* create_svg(char *s,int mx,int my,int n)
{FILE *f;
 int i;
 f=fopen(s,"w");
 if (f!=NULL) printf("\npohoda");
 printf("\n%d,%d",mx,my);
 fprintf(f,"<?xml version=\"1.0\"?>\n<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\"\n\"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n");
 fprintf(f,"<svg xmlns=\"http://www.w3.org/2000/svg\" width = \"%d\" height = \"%d\" version = \"1.1\">\n",mx,my);
 
 fprintf(f,"<line x1=\"%d\" x2=\"%d\" y1=\"%d\" y2=\"%d\" stroke=\"black\"/>\n",0,mx,my-20,my-20); // x os
 fprintf(f,"<line x1=\"%d\" x2=\"%d\" y1=\"%d\" y2=\"%d\" stroke=\"black\"/>\n",20,20,0,my); // y os
 return f;
}

void udaje_svg(FILE *f,POC *b,POC *c,int j,int krok,int my)
{int i;
 for(i=5;i<j-1;i++)
 {
  fprintf(f,"<line x1=\"%d\" x2=\"%d\" y1=\"%d\" y2=\"%d\"  stroke=\"red\"/>\n",2+i*(krok),2+(i+1)*(krok),my-20-b[i].porov,my-20-b[i+1].porov);
  fprintf(f,"<circle cx=\"%d\" cy=\"%d\" r=\"2\"   stroke=\"red\"/>\n",2+i*(krok),my-20-b[i].porov);
  fprintf(f,"<line x1=\"%d\" x2=\"%d\" y1=\"%d\" y2=\"%d\"  stroke=\"blue\" />\n",4+i*(krok),4+(i+1)*(krok),my-20-b[i].vymen,my-20-b[i+1].vymen);
  fprintf(f,"<circle cx=\"%d\" cy=\"%d\" r=\"2\"   stroke=\"blue\"/>\n",4+i*(krok),my-20-b[i].vymen);
  fprintf(f,"<line x1=\"%d\" x2=\"%d\" y1=\"%d\" y2=\"%d\"  stroke=\"black\" />\n",6+i*(krok),6+(i+1)*(krok),my-20-c[i].porov,my-20-c[i+1].porov);
  fprintf(f,"<circle cx=\"%d\" cy=\"%d\" r=\"2\"   stroke=\"black\"/>\n",6+i*(krok),my-20-c[i].porov);
  fprintf(f,"<line x1=\"%d\" x2=\"%d\" y1=\"%d\" y2=\"%d\"  stroke=\"green\" />\n",8+i*(krok),8+(i+1)*(krok),my-20-c[i].vymen,my-20-c[i+1].vymen);
  fprintf(f,"<circle cx=\"%d\" cy=\"%d\" r=\"2\"   stroke=\"green\"/>\n",8+i*(krok),my-20-c[i].vymen);
 }
 fprintf(f,"<text x=\"%d\" y=\"%d\" fill=\"black\">os x-->narastajuci pocet prvkov na triedenie</text>\n",21,my-5,i);
 fprintf(f,"<text x=\"%d\" y=\"%d\" fill=\"red\">COMB SORT->porovnania</text>\n",21,20,i);
 fprintf(f,"<text x=\"%d\" y=\"%d\" fill=\"blue\">COMB SORT->vymeny</text>\n",21,40,i);
 fprintf(f,"<text x=\"%d\" y=\"%d\" fill=\"black\">SHELL SORT->porovnania</text>\n",21,60,i);
 fprintf(f,"<text x=\"%d\" y=\"%d\" fill=\"green\">SHELL SORT->vymeny</text>\n",21,80,i);
}
void close_svg(FILE *f)
{
    fprintf(f,"\n</svg>");
    fclose(f);
}

void SHELLSORT(int *pole,int n,POC *c)
{int i,j,k,tmp;
c->porov=0;
c->vymen=0;
 for(i=n/2; i>0; i=i/2)
  {
    for(j=i; j<n; j++)
    {
     for(k=j-i; k>=0; k=k-i)
      {
         if(pole[k+i]>=pole[k])
             { c->porov++;
                break;
             }
         else
         {
             tmp=pole[k];
             pole[k]=pole[k+i];
             pole[k+i]=tmp;
             c->vymen+=3;
         }
      }
    }
  }
}

void MAXSORT(int *pole,int n,POC *a)
{int max,i,j,tmp;
 a->vymen=0;
 a->porov=0;
 for(i=0;i<n-1;i++)
 {
        max=i;
        for(j=i+1;j<n;j++)
        {
            if (pole[max]<pole[j]) { max=j;
                                     a->porov++;                                
                                     tmp=pole[max];
                                     pole[max]=pole[j];
                                     pole[j]=tmp;
                                     a->vymen=a->vymen+3;  
                                    }
         
        }
 }
}

void COMBSORT(int *pole,int n,POC *b)
{int medzera,i,j,k,tmp;
medzera=(n+0.5)/2;
b->porov=0;
b->vymen=0;
for(i=medzera;i>0;i--)
 {
        for(j=0;j<n-i;j++)
        {
            if(pole[j]>pole[j+i]) {
                                    b->porov=b->porov+1;
                                    tmp=pole[j];
                                    pole[j]=pole[j+i];
                                    pole[j+i]=tmp;
                                    b->vymen=b->vymen+3; 
                                  }
        }
 }    
    
}

main()
{
int n=70,i,j,mx,my,krok;
POC a[n],b[n],c[n];
int *pole1,*pole2,*pole3;
FILE *f;
mx=10*n;
krok=mx/n;
srand(time(NULL));
for(j=5;j<n;j++)
{
  pole1=(int*)malloc(j*sizeof(int));
  pole2=(int*)malloc(j*sizeof(int));
  pole3=(int*)malloc(j*sizeof(int));
  for(i=0;i<j;i++)
     {pole1[i]=rand()%100;
      pole2[i]=pole1[i];
      pole3[i]=pole1[i];
     }
  MAXSORT(pole1,j,&a[j]);
  printf("\n %d.MS v/p:%d,%d",j,a[j].vymen,a[j].porov);
  COMBSORT(pole2,j,&b[j]);
  printf("\n %d.CS v/p:%d,%d",j,b[j].vymen,b[j].porov);
  SHELLSORT(pole3,j,&c[j]);
  printf("\n %d.SS v/p:%d,%d",j,c[j].vymen,c[j].porov);
  printf("\n");
  if(a[j].porov>my) my=a[j].porov;
  if(a[j].vymen>my) my=a[j].vymen;
  if(b[j].porov>my) my=b[j].porov;
  if(b[j].vymen>my) my=b[j].vymen;
  if(c[j].porov>my) my=c[j].porov;
  if(c[j].vymen>my) my=c[j].vymen;
  if (j==n-1) {
                f=create_svg("moje_svg.svg",mx,my,n);
                udaje_svg(f,&*b,&*c,j,krok,my);
              }
  free(pole1);
  free(pole2);
  free(pole3);
}
close_svg(f);
system("Pause");   
}
